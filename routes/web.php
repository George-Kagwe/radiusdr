<?php
use App\Mail\TestMail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
// Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::get('register', 'Auth\RegisterController@register_home')->name('register');
Route::post('register', 'Auth\RegisterController@register_home');
Route::get('start', 'Auth\RegisterController@register_home')->name('start');
Route::get('business_registration', 'Auth\RegisterController@business')->name('business_registration');
Route::get('recovery_site_registration', 'Auth\RegisterController@recovery_site')->name('recovery_site_registration');
Route::get('supplier_registration', 'Auth\RegisterController@supplier')->name('supplier_registration');
Route::get('agent_registration', 'Auth\RegisterController@agent')->name('agent_registration');


// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/business', 'HomeController@business')->name('business');

Route::get('/agent', 'HomeController@agent')->name('agent');
Route::get('/supplier', 'HomeController@supplier')->name('supplier');
Route::get('/ses', 'HomeController@ses')->name('ses');
Route::get('/service_provider', 'HomeController@service_provider')->name('service_provider');


Route::get('/emails', 'MailSend@index')->name('emails');

Route::get('/send/email', 'HomeController@mail');
// Email related routes

Route::get('mail/send', 'MailController@send');

Route::get('/profile', 'ProfilesController@index')->name('profile');

Route::get('allusers/', 'Api\UsersController@index')->name('fetchUsers');
Route::post('users/store', array('as' => 'storeUser', 'uses' => 'Api\UsersController@store'));

