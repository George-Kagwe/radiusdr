<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>RADIUS DR</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <!-- <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css"> -->
      <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style type="text/css">
                            /*custom font*/
                        @import url(https://fonts.googleapis.com/css?family=Montserrat);

                        /*basic reset*/
                        * {
                            margin: 0;
                            padding: 0;
                        }

                        html {
                            height: 100%;
                            background: #6441A5; /* fallback for old browsers */
                            background: -webkit-linear-gradient(to left, #6441A5, #2a0845); /* Chrome 10-25, Safari 5.1-6 */
                            background: url('../img/landing-bg.jpg');
                        }

                        body {
                            font-family: montserrat, arial, verdana;
                            background: transparent;
                        }

                        /*form styles*/
                        #msform {
                            text-align: center;
                            position: relative;
                            margin-top: 30px;

                        }

                        #msform fieldset {
                            background: white;
                            border: 0 none;
                            border-radius: 0px;
                            box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
                            padding: 20px 30px;
                            box-sizing: border-box;
                            width: 80%;
                            margin: 0 10%;

                            /*stacking fieldsets above each other*/
                            position: relative;
                        }

                        /*Hide all except first fieldset*/
                        #msform fieldset:not(:first-of-type) {
                            display: none;
                        }

                        /*inputs*/
                        /*#msform input, #msform textarea {
                            padding: 15px;
                            border: 1px solid #ccc;
                            border-radius: 0px;
                            margin-bottom: 10px;
                            width: 100%;
                            box-sizing: border-box;
                            font-family: montserrat;
                            color: #2C3E50;
                            font-size: 13px;
                        }*/

                        #msform input:focus, #msform textarea:focus {
                            -moz-box-shadow: none !important;
                            -webkit-box-shadow: none !important;
                            box-shadow: none !important;
                            border: 1px solid #ee0979;
                            outline-width: 0;
                            transition: All 0.5s ease-in;
                            -webkit-transition: All 0.5s ease-in;
                            -moz-transition: All 0.5s ease-in;
                            -o-transition: All 0.5s ease-in;
                        }

                        /*buttons*/
                        #msform .action-button {
                            width: 100px;
                            background: #ee0979;
                            font-weight: bold;
                            color: white;
                            border: 0 none;
                            border-radius: 25px;
                            cursor: pointer;
                            padding: 10px 5px;
                            margin: 10px 5px;
                        }

                        #msform .action-button:hover, #msform .action-button:focus {
                            box-shadow: 0 0 0 2px white, 0 0 0 3px #ee0979;
                        }

                        #msform .action-button-previous {
                            width: 100px;
                            background: #C5C5F1;
                            font-weight: bold;
                            color: white;
                            border: 0 none;
                            border-radius: 25px;
                            cursor: pointer;
                            padding: 10px 5px;
                            margin: 10px 5px;
                        }

                        #msform .action-button-previous:hover, #msform .action-button-previous:focus {
                            box-shadow: 0 0 0 2px white, 0 0 0 3px #C5C5F1;
                        }

                        /*headings*/
                        .fs-title {
                            font-size: 18px;
                            text-transform: uppercase;
                            color: #2C3E50;
                            margin-bottom: 10px;
                            letter-spacing: 2px;
                            font-weight: bold;
                        }

                        .fs-subtitle {
                            font-weight: normal;
                            font-size: 13px;
                            color: #666;
                            margin-bottom: 20px;
                        }

                        /*progressbar*/
                        #progressbar {
                            margin-bottom: 30px;
                            overflow: hidden;
                            /*CSS counters to number the steps*/
                            counter-reset: step;
                             /*background: #6441A5;  fallback for old browsers */
                            /*background: -webkit-linear-gradient(to left, #6441A5, #2a0845); /* Chrome 10-25, Safari 5.1-6 */
                            /*background: url('hero-bg.jpg');*/
                        }

                        #progressbar li {
                            list-style-type: none;
                            color: white;
                            text-transform: uppercase;
                            font-size: 9px;
                            width: 20%;
                            float: left;
                            position: relative;
                            letter-spacing: 1px;
                        }

                        #progressbar li:before {
                            content: counter(step);
                            counter-increment: step;
                            width: 24px;
                            height: 24px;
                            line-height: 26px;
                            display: block;
                            font-size: 12px;
                            color: #333;
                            background: white;
                            border-radius: 25px;
                            margin: 0 auto 10px auto;
                        }

                        /*progressbar connectors*/
                        #progressbar li:after {
                            content: '';
                            width: 100%;
                            height: 2px;
                            background: white;
                            position: absolute;
                            left: -50%;
                            top: 9px;
                            z-index: -1; /*put it behind the numbers*/
                        }

                        #progressbar li:first-child:after {
                            /*connector not needed before the first step*/
                            content: none;
                        }

                        /*marking active/completed steps green*/
                        /*The number of the step and the connector before it = green*/
                        #progressbar li.active:before, #progressbar li.active:after {
                            background: #ee0979;
                            color: white;
                        }


                        /* Not relevant to this form */
                        .dme_link {
                            margin-top: 30px;
                            text-align: center;
                        }
                        .dme_link a {
                            background: #FFF;
                            font-weight: bold;
                            color: #ee0979;
                            border: 0 none;
                            border-radius: 25px;
                            cursor: pointer;
                            padding: 5px 25px;
                            font-size: 12px;
                        }

                        .dme_link a:hover, .dme_link a:focus {
                            background: #C5C5F1;
                            text-decoration: none;
                        }
                        .choice{
                            display: none;
                        }
                        #bcp{
                            display: none;
                        }
                        label{
                            text-align: left;
                        }

    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Radius') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->email }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">

           @yield('content')
        </main>











<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
           <script type="text/javascript">
$(document).ready(function(){
    $("#bcp").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            
            if(optionValue){
                $(".choice").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else{
                $(".choice").hide();
            }
        });
    }).change();
});
</script>
    <script type="text/javascript">
        
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function(){
    if(animating) return false;
    animating = true;
    
    current_fs = $(this).parent();
    next_fs = $(this).parent().next();
    
    //activate next step on progressbar using the index of next_fs
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
    
    //show the next fieldset
    next_fs.show(); 
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50)+"%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
        'transform': 'scale('+scale+')',
        'position': 'absolute'
      });
            next_fs.css({'left': left, 'opacity': opacity});
        }, 
        duration: 800, 
        complete: function(){
            current_fs.hide();
            animating = false;
        }, 
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$(".previous").click(function(){
    if(animating) return false;
    animating = true;
    
    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();
    
    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
    
    //show the previous fieldset
    previous_fs.show(); 
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1-now) * 50)+"%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({'left': left});
            previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
        }, 
        duration: 800, 
        complete: function(){
            current_fs.hide();
            animating = false;
        }, 
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$(".submit").click(function(){
    return false;
})
$('#notsure').change(function(){
    $('#bcp').toggle($('#notsure:checked').length > 0);
});

 
    </script>

</body>
</html>