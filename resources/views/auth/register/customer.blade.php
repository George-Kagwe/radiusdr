<!DOCTYPE html>
<html>
<head>
	   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>

   <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="{{ asset('css/multiselect.css') }}">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<style type="text/css">
	/*custom font*/
@import url(https://fonts.googleapis.com/css?family=Montserrat);

/*basic reset*/
* {
    margin: 0;
    padding: 0;
}
.navbar-default .navbar-nav>li>a {
           color: #000 !important;
                
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
html {
    height: 100%;
    background: #6441A5; /* fallback for old browsers */
    background: -webkit-linear-gradient(to left, #6441A5, #2a0845); /* Chrome 10-25, Safari 5.1-6 */
     background: url('../img/landing-bg.jpg');
}

body {
     margin-top: 70px;
    font-family: montserrat, arial, verdana;
    background: transparent;
}

/*form styles*/
#msform {
    text-align: center;
    position: relative;
    margin-top: 30px;

}

#msform fieldset {
    background: white;
    border: 0 none;
    border-radius: 0px;
    box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
    padding: 20px 30px;
    box-sizing: border-box;
    width: 80%;
    margin: 0 10%;

    /*stacking fieldsets above each other*/
    position: relative;
}

/*Hide all except first fieldset*/
#msform fieldset:not(:first-of-type) {
    display: none;
}

/*inputs*/
/*#msform input, #msform textarea {
    padding: 15px;
    border: 1px solid #ccc;
    border-radius: 0px;
    margin-bottom: 10px;
    width: 100%;
    box-sizing: border-box;
    font-family: montserrat;
    color: #2C3E50;
    font-size: 13px;
}*/

#msform input:focus, #msform textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: 1px solid #ee0979;
    outline-width: 0;
    transition: All 0.5s ease-in;
    -webkit-transition: All 0.5s ease-in;
    -moz-transition: All 0.5s ease-in;
    -o-transition: All 0.5s ease-in;
}

/*buttons*/
#msform .action-button {
    width: 100px;
    background: #ee0979;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 25px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px;
}

#msform .action-button:hover, #msform .action-button:focus {
    box-shadow: 0 0 0 2px white, 0 0 0 3px #ee0979;
}

#msform .action-button-previous {
    width: 100px;
    background: #C5C5F1;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 25px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px;
}

#msform .action-button-previous:hover, #msform .action-button-previous:focus {
    box-shadow: 0 0 0 2px white, 0 0 0 3px #C5C5F1;
}

/*headings*/
.fs-title {
    font-size: 18px;
    text-transform: uppercase;
    color: #2C3E50;
    margin-bottom: 10px;
    letter-spacing: 2px;
    font-weight: bold;
}

.fs-subtitle {
    font-weight: normal;
    font-size: 13px;
    color: #666;
    margin-bottom: 20px;
}

/*progressbar*/
#progressbar {
    margin-bottom: 30px;
    overflow: hidden;
    /*CSS counters to number the steps*/
    counter-reset: step;
     /*background: #6441A5;  fallback for old browsers */
    /*background: -webkit-linear-gradient(to left, #6441A5, #2a0845); /* Chrome 10-25, Safari 5.1-6 */
    /*background: url('hero-bg.jpg');*/
}

#progressbar li {
    list-style-type: none;
    color: white;
    text-transform: uppercase;
    font-size: 9px;
    width: 20%;
    float: left;
    position: relative;
    letter-spacing: 1px;
}

#progressbar li:before {
    content: counter(step);
    counter-increment: step;
    width: 24px;
    height: 24px;
    line-height: 26px;
    display: block;
    font-size: 12px;
    color: #333;
    background: white;
    border-radius: 25px;
    margin: 0 auto 10px auto;
}

/*progressbar connectors*/
#progressbar li:after {
    content: '';
    width: 100%;
    height: 2px;
    background: white;
    position: absolute;
    left: -50%;
    top: 9px;
    z-index: -1; /*put it behind the numbers*/
}

#progressbar li:first-child:after {
    /*connector not needed before the first step*/
    content: none;
}

/*marking active/completed steps green*/
/*The number of the step and the connector before it = green*/
#progressbar li.active:before, #progressbar li.active:after {
    background: #ee0979;
    color: white;
}


/* Not relevant to this form */
.dme_link {
    margin-top: 30px;
    text-align: center;
}
.dme_link a {
    background: #FFF;
    font-weight: bold;
    color: #ee0979;
    border: 0 none;
    border-radius: 25px;
    cursor: pointer;
    padding: 5px 25px;
    font-size: 12px;
}

.dme_link a:hover, .dme_link a:focus {
    background: #C5C5F1;
    text-decoration: none;
}
.choice{
	display: none;
}
#bcp{
	display: none;
}
label{
	text-align: left;
}
#accessories{
	width:120vw !important;
}
#totalaccessories{
	font-weight: bold;
}
#totalamenities{
	font-weight: bold;
}
.btn-link{
    color: red;
    font-weight: bold;
}
</style>
</head>
<body>
<!-- nav bar -->
            <nav class="navbar navbar-default navbar-fixed-top">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="{{ url('/') }}">Radius Dr</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                    
                  </ul>
                  
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                     
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>
<!-- end navbar -->
	<!-- MultiStep Form -->
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <form id="msform">
            <!-- progressbar -->
            <ul id="progressbar">
                <li class="active">Company Details</li>
                <li>Add Accessories</li>
                <li>Choose Amenities</li>
                <li>Account Setup</li>
                <li>Checkout</li>
            </ul>
        
             
            <!-- fieldsets -->

            <fieldset>
                <h2 class="fs-title">Company Details</h2>
                <h3 class="fs-subtitle">Tell us something more about your company</h3>
                	<div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">What is Your Company Name?</label>
                 <div class="col-md-8">
                <input type="text" name="employees"  class="form-control" placeholder="" required="true" />
                 </div>
             </div>
             <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">How many employees are at your company?</label>
                 <div class="col-md-8">
                <input type="text" name="employees"  class="form-control" placeholder="" required="true" />
                 </div>
             </div>
             <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">How many desks do you need?</label>
                 <div class="col-md-4">
                    <input type="text" name="employees"  class="form-control" placeholder="" required="true" />
                 </div>
                 <div class="col-md-4">
                   <label><input type="checkbox" value="" id="notsure">  Am Not Sure</label>
                 </div>
             </div>

             <div class="form-group row" id="bcp">
                <label for="name" class="col-md-4 col-form-label text-md-right">Do you have a BCP Plan?</label>
                 <div class="col-md-8">
                   <select class="form-control" id="selection">
						    <option>Please select Yes or No</option>
						    <option value="justhelp">Yes</option>
						    <option  value="helpchoose">No</option>
				   </select>
                 </div>
                
             </div>
                <div class="choice helpchoose">
              <div class="form-group row ">
                <label for="name" class="col-md-4 col-form-label text-md-right"> We can help you choose for 30 pounds. Are you interested?</label>
                 <div class="col-md-8">
                   <select class="form-control" id="sel1">
						    <option>Please select Yes or No</option>
						    <option>Yes</option>
						    <option>No</option>
				   </select>
                 </div>
                
             </div>
            </div>
             <div class="choice justhelp">
             <div class="form-group row ">
                <label for="name" class="col-md-4 col-form-label text-md-right"> I need just help</label>
                 <div class="col-md-8">
                   <select class="form-control" id="jh">
						    <option>Please select Yes or No</option>
						    <option>Yes</option>
						    <option>No</option>
				   </select>
                 </div>
                
             </div>
             </div>
                
                <input type="button" name="next" class="next action-button" value="Next"/>
           
            </fieldset>
            
              <fieldset>
                <h2 class="fs-title">Add Accessories</h2>
                <h3 class="fs-subtitle">Please tell us what accessories you need</h3>
                 <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">Please choose as many accessories as you need</label>
                 <div class="col-md-4">
                   <select class="form-control" id="accessories" multiple="multiple">
						    <option value="1">Option 1</option>
						    <option value="2" >Option 2</option>
						    <option value="3" >Option 3</option>
						    <option value="4">Option 4</option>
						    <option value="5">Option 5</option>
						    <option value="6">Option 6</option>
				   </select>
                 </div>
                 <div class="col-md-4">
                      <div id="totalaccessories"></div>
                 </div>

                
             </div>
                <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                <input type="button" name="next" class="next action-button" value="Next"/>
            </fieldset>
            <fieldset>
                <h2 class="fs-title">Select Amenities needed</h2>
                <h3 class="fs-subtitle">Please tell us what amenities you need</h3>
                 <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">Please choose as many amenities as you need</label>
                 <div class="col-md-4">
                   <select class="form-control" id="amenities" multiple="multiple">
						    <option value="1">Option 1</option>
						    <option value="2" >Option 2</option>
						    <option value="3" >Option 3</option>
						    <option value="4">Option 4</option>
						    <option value="5">Option 5</option>
						    <option value="6">Option 6</option>
				   </select>
                 </div>

                 <div class="col-md-4">
                      <div id="totalamenities"></div>
                 </div>
                
             </div>
                <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                <input type="button" name="next" class="next action-button" value="Next"/>
            </fieldset>
             <fieldset>
                <h2 class="fs-title">Account Setup</h2>
                <h3 class="fs-subtitle">Please setup your account</h3>
                
             <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">Company Email</label>
                 <div class="col-md-8">
                    <input type="text" name="employees"  class="form-control" placeholder="" required="true" />
                 </div>
                 
             </div>
             <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">Company Phone</label>
                 <div class="col-md-8">
                    <input type="text" name="employees"  class="form-control" placeholder="" required="true" />
                 </div>
                 
             </div>
             <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">Password</label>
                 <div class="col-md-8">
                    <input type="text" name="employees"  class="form-control" placeholder="" required="true" />
                 </div>
                 
             </div>
             <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">Confirm Password</label>
                 <div class="col-md-8">
                    <input type="text" name="employees"  class="form-control" placeholder="" required="true" />
                 </div>
                 
             </div>
              <div class="form-group row">
               
                 <div class="col-md-4">
                   
                 </div>
                 <div class="col-md-4">
                    <button type="button" class="btn btn-link" data-toggle="modal" data-target="#TCPP"
                    >Read our terms and conditions and Privacy Policy</button>
                   <label><input type="checkbox" value="" id="terms"> I agree with terms and conditions </label>
                 </div>
             </div>
                <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                <input type="button" name="next" class="next action-button" value="Next"/>
            </fieldset>
            <fieldset>
                <h2 class="fs-title">Please Pay</h2>
                <h3 class="fs-subtitle">Fill in your credentials</h3>
                
                <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                <input type="submit" name="submit" class="submit action-button" value="Submit"/>
            </fieldset>

          
        </form>
        <!-- link to designify.me code snippets -->
        <!-- <div class="dme_link">
            <p><a href="http://designify.me/code-snippets-js/" target="_blank">More Code Snippets</a></p>
        </div> -->
        <!-- /.link to designify.me code snippets -->
    </div>
</div>
<!-- /.MultiStep Form -->

<!-- Terms and Conditions and Privacy Policy  Modal-->
<div id="TCPP" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">RADIUS DR Terms and conditions and Privacy Policy</h4>
      </div>
      <div class="modal-body">
        <h1>TERMS AND CONDITIONS</h1>
        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
        <h1>OUR PRIVACY POLICY</h1>
        <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src=" {{ asset('js/multiselect.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

        <script type="text/javascript">
                $('#accessories').multiselect({
                	buttonWidth: '20vw'
                });
                 $('#amenities').multiselect({
                 	buttonWidth: '20vw'
                 });
       </script>

       <script type="text/javascript">
       	$('#accessories').change(function(){
    var sum = 0;
    $('#accessories :selected').each(function() {
        sum += Number($(this).val());
    });
     $("#totalaccessories").html('   Cost:  £ '+sum);
     });

       		$('#amenities').change(function(){
    var sum = 0;
    $('#amenities :selected').each(function() {
        sum += Number($(this).val());
    });
     $("#totalamenities").html('   Cost:  £ '+sum);
     });
       </script>

           <script type="text/javascript">

$(document).ready(function(){
    $("#bcp").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            
            if(optionValue){
                $(".choice").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else{
                $(".choice").hide();
            }
        });
    }).change();
});
</script>
    <script type="text/javascript">
		
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	next_fs = $(this).parent().next();
	
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
	//show the next fieldset
	next_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 50)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({
        'transform': 'scale('+scale+')',
        'position': 'absolute'
      });
			next_fs.css({'left': left, 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".submit").click(function(){
	return false;
})
$('#notsure').change(function(){
    $('#bcp').toggle($('#notsure:checked').length > 0);
});

 
    </script>

</body>
</html>