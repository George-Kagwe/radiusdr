<!DOCTYPE html>
<html>
<head>
       <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

   <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
 <style type="text/css">
       html{
         background: url('../img/landing-bg.jpg');
       }
       body{
        margin-top: 100px;
         background: transparent !important;
       }
        .card{
            background: white;
            border: 0 none;
            border-radius: 0px;
            box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
            padding:10px;
            box-sizing: border-box;
               
       }
       .row{
         margin-bottom: 3%;
       }
       .card h3{
        text-align: center;
       }

       .card p{
         margin-left: 3px;
       }
        .btn-success{
         width: 110px;
         background: #ee0979;
         font-weight: bold;
         color: white;
         border: 0 none;
         border-radius: 25px;
         cursor: pointer;
         padding: 10px 5px;
         margin: 10px 5px;
         margin-left: 27%;
       }
         .btn-success:hover, .btn-success:focus {
           box-shadow: 0 0 0 2px white, 0 0 0 3px #C5C5F1;
         }

        h3{
         font-size: 18px;
         text-transform: uppercase;
         color: #2C3E50;
         margin-bottom: 10px;
         letter-spacing: 2px;
         font-weight: bold;
       }
 </style>
</head>
<body>
<!-- nav bar -->
            <nav class="navbar navbar-default navbar-fixed-top">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="{{ url('/') }}">Radius Dr</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                    
                  </ul>
                  
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>
<!-- end navbar -->
        <div class="container-fluid">
              <div  class="row">
                    <div id="cards">
                        <div class="col-lg-3">
                        <div class="card">
                                  
                                  <div class="card-body">
                                    <h3 class="card-title">For Businesses</h3>
                                    
                                    <p class="card-text">I am a Business looking for a BCP site Recovery</p>
                                    <a href="/business_registration" class="btn btn-success active">
                                                                Register Here
                                                           </a> 

                                  </div>
                             </div> 
                        </div>
                        <div class="col-lg-3">
                             <div class="card">

                                  <div class="card-body">
                                    <h3 class="card-title">For Recovery Sites</h3>
                                    
                                    <p class="card-text">I am a Recovery site renting out desks and office space</p>
                                    <a href="/recovery_site_registration" class="btn btn-success active">
                                                                Register Here
                                                           </a> 
                                  </div>
                             </div> 
                        </div>
                    
                  
                        
                   </div>
                   
              </div>
              <div  class="row">
                    <div id="cards">
                        
                  
                        <div class="col-lg-3">
                             <div class="card">
                                 
                                  <div class="card-body">
                                    <h3 class="card-title">For Suppliers</h3>
                                 
                                    <p class="card-text">I am a supplier looking to offer products and services</p>
                                    <a href="/supplier_registration" class="btn btn-success active">
                                                                Register Here
                                                           </a> 
                                  </div>
                             </div> 
                        </div>
                        <div class="col-lg-3">
                             <div class="card">
                                  
                                  <div class="card-body">
                                    <h3 class="card-title">For Agents</h3>
                                 
                                    <p class="card-text">I am an agent looking to sell Radius Dr Services</p>
                                    <a href="/agent_registration" class="btn btn-success active">
                                                                Register Here
                                                           </a> 
                                  </div>
                             </div> 
                        </div>
                   </div>
                   
              </div>
        </div>
       
  





</body>
</html>