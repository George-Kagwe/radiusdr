<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use Response;
use App\Models\SuppliersModel;
use View;



class SuppliersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     protected $rules =
    [
      'email'=>'required|email',
      'phone_number'=>'required',
      'business_name'=>'required',
      'industry'=>'required',
       'no_of_locations'=>'required|numeric',
       'no_of_employees'=>'required|numeric',
       'no_of_years'=>'required|numeric',
       'contact_name'=>'required',
       'contact_email'=>'required',
      'postal_code'=>'required',
      'location'=>'required',
      'desks'=>'required',  
                                                
                                                    
                        
    ];
    public function index()
    {
        
        $get_all_suppliers =SuppliersModel::all();
        
          echo json_encode($get_all_suppliers);    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = \Validator::make($request->all(), [
            'email'=>'required|email',
            'phone_number'=>'required',
            'business_name'=>'required',
            'industry'=>'required',
             'no_of_locations'=>'required|numeric',
             'no_of_employees'=>'required|numeric',
             'no_of_years'=>'required|numeric',
             'contact_name'=>'required',
             'contact_email'=>'required',
            'postal_code'=>'required',
            'location'=>'required',
            'desks'=>'required',            
        ]);
        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        else{
            $supplier = new SuppliersModel();
            $supplier->email =$request->email;
            $supplier->phone_number=$request->phone_number;
            $supplier->business_name=$request->business_name;
            $supplier->no_of_locations=$request->no_of_locations;
            $supplier->no_of_employees=$request->no_of_employees;
            $supplier->no_of_years=$request->no_of_years;
            $supplier->contact_name=$request->contact_name;
            $supplier->contact_email=$request->contact_email;
            $supplier->postal_code=$request->postal_code;
            $supplier->location=$request->location;
            $supplier->desks=$request->desks;


            $supplier->save();
             return response()->json($supplier);
           echo json_encode(array("status" => TRUE));

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($supplier_id)
    {
       
         
         $supplier = suppliers_Model::findOrfail($supplier_id);

  
          echo json_encode($supplier);     
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
                
          $validator = \Validator::make($request->all(), [
            'email'=>'required|email',
            'phone_number'=>'required',
            'business_name'=>'required',
            'industry'=>'required',
             'no_of_locations'=>'required|numeric',
             'no_of_employees'=>'required|numeric',
             'no_of_years'=>'required|numeric',
             'contact_name'=>'required',
             'contact_email'=>'required',
            'postal_code'=>'required',
            'location'=>'required',
            'desks'=>'required',            
                              
        ]);
        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        else{
         
            $supplier =SuppliersModel::find($request->id);
            $supplier->email =$request->email;
            $supplier->phone_number=$request->phone_number;
            $supplier->business_name=$request->business_name;
            $supplier->no_of_locations=$request->no_of_locations;
            $supplier->no_of_employees=$request->no_of_employees;
            $supplier->no_of_years=$request->no_of_years;
            $supplier->contact_name=$request->contact_name;
            $supplier->contact_email=$request->contact_email;
            $supplier->postal_code=$request->postal_code;
            $supplier->location=$request->location;
            $supplier->desks=$request->desks;
            $supplier->save();
             return response()->json($supplier);
           echo json_encode(array("status" => TRUE));

        }  

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
