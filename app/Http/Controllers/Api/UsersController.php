<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use Response;
use App\Models\UsersModel;
use View;



class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     protected $rules =
    [
      'email'=>'required|email',
      'phone_number'=>'required',
      'business_name'=>'required',
      'industry'=>'required',
       'no_of_locations'=>'required|numeric',
       'no_of_employees'=>'required|numeric',
       'no_of_years'=>'required|numeric',
       'contact_name'=>'required',
       'contact_email'=>'required',
      'postal_code'=>'required',
      'location'=>'required',
      'desks'=>'required',                                              
                                                    
                        
    ];
    public function index()
    {
        
        $get_all_users =UsersModel::all();
        
          echo json_encode($get_all_users);    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = \Validator::make($request->all(), [
            'email'=>'required|email',
            'phone_number'=>'required',
            'business_name'=>'required',
            'industry'=>'required',
             'no_of_locations'=>'required|numeric',
             'no_of_employees'=>'required|numeric',
             'no_of_years'=>'required|numeric',
             'contact_name'=>'required',
             'contact_email'=>'required',
            'postal_code'=>'required',
            'location'=>'required',
            'desks'=>'required',            
        ]);
        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        else{
            $user = new UsersModel();
            $user->email =$request->email;
            $user->phone_number=$request->phone_number;
            $user->business_name=$request->business_name;
            $user->no_of_locations=$request->no_of_locations;
            $user->no_of_employees=$request->no_of_employees;
            $user->no_of_years=$request->no_of_years;
            $user->contact_name=$request->contact_name;
            $user->contact_email=$request->contact_email;
            $user->postal_code=$request->postal_code;
            $user->location=$request->location;
            $user->desks=$request->desks;


            $user->save();
             return response()->json($user);
           echo json_encode(array("status" => TRUE));

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
       
         
         $user = Users_Model::findOrfail($user_id);

  
          echo json_encode($user);     
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
                
          $validator = \Validator::make($request->all(), [
            'email'=>'required|email',
            'phone_number'=>'required',
            'business_name'=>'required',
            'industry'=>'required',
             'no_of_locations'=>'required|numeric',
             'no_of_employees'=>'required|numeric',
             'no_of_years'=>'required|numeric',
             'contact_name'=>'required',
             'contact_email'=>'required',
            'postal_code'=>'required',
            'location'=>'required',
            'desks'=>'required',            
                              
        ]);
        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        else{
         
            $user =UsersModel::find($request->id);
            $user->email =$request->email;
            $user->phone_number=$request->phone_number;
            $user->business_name=$request->business_name;
            $user->no_of_locations=$request->no_of_locations;
            $user->no_of_employees=$request->no_of_employees;
            $user->no_of_years=$request->no_of_years;
            $user->contact_name=$request->contact_name;
            $user->contact_email=$request->contact_email;
            $user->postal_code=$request->postal_code;
            $user->location=$request->location;
            $user->desks=$request->desks;
            $user->save();
             return response()->json($user);
           echo json_encode(array("status" => TRUE));

        }  

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
