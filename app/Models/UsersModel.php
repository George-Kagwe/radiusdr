<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersModel extends Model
{



 
    protected $primaryKey = 'user_id';
    protected $table ='userss';
    protected $fillable =[
        'email',
        'phone_number',
        'business_name',
        'industry',
         'no_of_locations',
         'no_of_employees',
         'no_of_years',
         'contact_name',
         'contact_email',
        'postal_code',
        'location',
        'desks',            
                         ];


}
